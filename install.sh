#!/usr/bin/env bash

# Colors
d="\033[39m"
r="\033[31m"
y="\033[33m"
b="\033[34m"
g="\033[32m"

now=$(date --iso-8601='second')

# Base vimrc installation
printf "${b}Checking if .vimrc already exists...${d}\n"
if [ -f $HOME/.vimrc ]; then
    printf "${y}Found .vimrc in $HOME. Renaming to vimrc-$now.bak${d}\n"
    mv $HOME/.vimrc $HOME/vimrc-$now.bak
fi
printf "${b}Copying $(pwd)/vimrc to $HOME/.vimrc${d}\n"
cp vimrc $HOME/.vimrc

# Bundles/plugins installation
printf "${b}Checking if Vundle bundles/plugins need to be copied${d}\n"
if [ -d bundle ]; then
    printf "${g}Bundle directory detected. Copying to $HOME/.vim ... ${d}\n"
    mkdir -p $HOME/.vim
    cp -rf bundle $HOME/.vim/
fi

printf "${g}Done${d}\n"
