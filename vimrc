set shiftwidth=4
set tabstop=4
set expandtab

inoremap jk <esc>
inoremap JK <esc>

" Save, compile, and run rust file from editor
autocmd filetype rust nnoremap <F4> :w <bar> exec '!rustc '.shellescape('%').' && ./'.shellescape('%:r')<CR>

color desert

syntax on

" Easy .vimrc editing
"let mapleader = ','
let maplocalleader = ","
nnoremap <localleader>ev :vsplit ~/.vimrc<cr>
nnoremap <localleader>sv :source ~/.vimrc<cr>

" Set paste mode
nnoremap <localleader>pp :set paste!<cr>

" Turn on highlighting during search
set hlsearch

set autoindent

" Toggle line numbers
nnoremap <localleader>nn :set number!<cr>

" Toggle search highlight
nnoremap <localleader>hh :set hlsearch!<cr>

" Make search case insensitive by default
set ignorecase

" Toggle paste mode with F4
set pastetoggle=<F3>


" Allows pasting from clipboard without setting paste mode in tmux
" See https://coderwall.com/p/if9mda/automatically-set-paste-mode-in-vim-when-pasting-in-insert-mode
function! WrapForTmux(s)
  if !exists('$TMUX')
    return a:s
  endif

  let tmux_start = "\<Esc>Ptmux;"
  let tmux_end = "\<Esc>\\"

  return tmux_start . substitute(a:s, "\<Esc>", "\<Esc>\<Esc>", 'g') . tmux_end
endfunction

let &t_SI .= WrapForTmux("\<Esc>[?2004h")
let &t_EI .= WrapForTmux("\<Esc>[?2004l")

function! XTermPasteBegin()
  set pastetoggle=<Esc>[201~
  set paste
  return ""
endfunction

inoremap <special> <expr> <Esc>[200~ XTermPasteBegin()

" Adds fzf completion with CTRL+t
set rtp+=/usr/local/opt/fzf
